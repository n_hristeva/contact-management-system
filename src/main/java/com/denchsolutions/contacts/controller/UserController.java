package com.denchsolutions.contacts.controller;

import com.denchsolutions.contacts.dto.RegisterUserDTO;
import com.denchsolutions.contacts.exception.BadRequestRestException;
import com.denchsolutions.contacts.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {this.userService = userService;}

    @PostMapping("/register")
    public void register(@Valid @RequestBody RegisterUserDTO registerUserDTO) {
        if (userService.existsByUsername(registerUserDTO.getUsername())) {
            throw new BadRequestRestException("Username is already taken.");
        }

        userService.registerUser(registerUserDTO);
    }
}
