package com.denchsolutions.contacts.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    private final PasswordEncoder passwordEncoder;

    public MainController(PasswordEncoder passwordEncoder) {this.passwordEncoder = passwordEncoder;}

    @GetMapping("/hello-world")
    @PreAuthorize("hasRole('ADMIN')")
    public String helloWorld() {
        return "Hello Admin!";
    }

    //Display how 'admin' is encrypted in order to create in DB proper password for admin user.
    @GetMapping("/api/register")
    public String register() {
        return passwordEncoder.encode("admin");
    }
}
