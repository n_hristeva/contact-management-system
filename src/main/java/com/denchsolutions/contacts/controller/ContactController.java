package com.denchsolutions.contacts.controller;

import com.denchsolutions.contacts.dto.ContactDTO;
import com.denchsolutions.contacts.dto.CreateContactDTO;
import com.denchsolutions.contacts.dto.UpdateContactDTO;
import com.denchsolutions.contacts.exception.BadRequestRestException;
import com.denchsolutions.contacts.exception.NoSuchResourceException;
import com.denchsolutions.contacts.exception.NotFoundRestException;
import com.denchsolutions.contacts.exception.UnauthorizedRestException;
import com.denchsolutions.contacts.service.ContactService;
import com.denchsolutions.contacts.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.Collection;

@RestController
@RequestMapping(("/api/contacts"))
public class ContactController {
    private final UserService userService;
    private final ContactService contactService;
    private final KafkaTemplate<String, String> kafkaTemplate;

    ContactController(UserService userService, ContactService contactService, KafkaTemplate<String, String> kafkaTemplate) {
        this.userService = userService;
        this.contactService = contactService;
        this.kafkaTemplate = kafkaTemplate;
    }

    @GetMapping
    public Collection<ContactDTO> getAllContacts() {
        try {
            return contactService.getContactsByUserId(getAuthenticatedUserId());
        } catch (NoSuchResourceException e) {
            throw new NotFoundRestException(e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('USER_WRITE')")
    @ResponseStatus(HttpStatus.CREATED)
    public void addContact(@Valid @RequestBody CreateContactDTO createContactDTO) {
        try {
            contactService.createContact(createContactDTO, getAuthenticatedUserId());
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    @PatchMapping(("/{id}") )
    @PreAuthorize("hasRole('USER_WRITE')")
    public void updateContact(@Valid @RequestBody UpdateContactDTO updateContactDTO, @PathVariable("id") long contactId) {
        try {
          contactService.updateContact(updateContactDTO,getAuthenticatedUserId(), contactId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    @DeleteMapping(("/{id}") )
    @PreAuthorize("hasRole('USER_WRITE')")
    public void deleteContact( @PathVariable("id") long contactId) {
        try {
            contactService.deleteContact(getAuthenticatedUserId(), contactId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }
    //Kafka endpoint
    @PostMapping(("/{id}/click") )
    public void click(@PathVariable("id") long contactId) {

        kafkaTemplate.send("userTopic", String.valueOf(contactId));
    }

    private Long getAuthenticatedUserId() {
        final Authentication authentication = SecurityContextHolder.getContext()
            .getAuthentication();

        if (authentication == null) {
            throw new UnauthorizedRestException();
        }

        User authenticatedUser = (User) authentication.getPrincipal();
        return userService.findByUsername(authenticatedUser.getUsername())
            .getId();
    }
}
