package com.denchsolutions.contacts.controller;

import com.denchsolutions.contacts.dto.ContactDTO;
import com.denchsolutions.contacts.dto.CreateContactDTO;
import com.denchsolutions.contacts.dto.RegisterUserDTO;
import com.denchsolutions.contacts.dto.UpdateAsAdminUserDTO;
import com.denchsolutions.contacts.dto.UpdateContactDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminContactDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminUserDTO;
import com.denchsolutions.contacts.exception.BadRequestRestException;
import com.denchsolutions.contacts.exception.NoSuchResourceException;
import com.denchsolutions.contacts.exception.NotFoundRestException;
import com.denchsolutions.contacts.model.User;
import com.denchsolutions.contacts.service.ContactService;
import com.denchsolutions.contacts.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.Collection;

@RestController
@RequestMapping(("/api/admin"))
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {
    private final UserService userService;
    private final ContactService contactService;

    public AdminController(UserService userService, ContactService contactService) {
        this.userService = userService;
        this.contactService = contactService;
    }

    //CONTACTS

    //find all contacts
    @GetMapping(("/contacts"))
    public Collection<ViewAsAdminContactDTO> getAllContacts() {
        try {
            return contactService.getContactsAsAdmin();
        } catch (NoSuchResourceException e) {
            throw new NotFoundRestException(e.getMessage());
        }
    }

    //find by id of contact
    //Q: if we want to display the user of the contact, should we create a new DTO class (ContactUserDTO)?
    @GetMapping(("/contacts/{id}"))
    public ContactDTO getContactByContactId(@PathVariable("id") long contactId) {
        try {
            return contactService.findContactById(contactId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //delete by id of contact
    @DeleteMapping(("/contacts/{id}"))
    public void deleteContactByContactId(@PathVariable("id") long contactId) {
        try {
            contactService.deleteContactByContactId(contactId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //update by id of contact
    @PatchMapping(("/contacts/{id}"))
        public void updateContactByContactId(@Valid @RequestBody UpdateContactDTO updateContactDTO, @PathVariable("id") long contactId) {
        try {
            contactService.updateContactByContactId(updateContactDTO, contactId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //USERS

    //find all users
    @GetMapping(("/users"))
    public Collection<ViewAsAdminUserDTO> getAllUsers() {
        try {
            return userService.getUsersAsAdmin();
        } catch (NoSuchResourceException e) {
            throw new NotFoundRestException(e.getMessage());
        }
    }

    //find user by id
    @GetMapping(("/users/{id}"))
    public ViewAsAdminUserDTO getUserByUserIdAsAdmin(@PathVariable("id") long userId) {
        try {
            return userService.findUserAsAdmin(userId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //delete user by id and delete the user`s contacts
    @DeleteMapping(("/users/{id}"))
    public void deleteUserByUserId(@PathVariable("id") long userId) {
        try {
            userService.deleteUserByUserId(userId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //update user by id
    @PatchMapping(("/users/{id}"))
    public void updateUserByUserId(@Valid @RequestBody UpdateAsAdminUserDTO adminUserDTO, @PathVariable("id") long userId) {
        try {
            userService.updateUserByUserId(adminUserDTO, userId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //create user
    @PostMapping(("/users"))
    public User createUser(@Valid @RequestBody RegisterUserDTO registerUserDTO) {
        try {
            return userService.registerUser(registerUserDTO);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }

    //create to a specific user a contact
    @PostMapping(("/users/{id}/contacts"))
    public void createAContactToSpecificUser(@Valid @RequestBody CreateContactDTO createContactDTO, @PathVariable("id") long userId) {
        try {
            contactService.createContact(createContactDTO, userId);
        } catch (NoSuchResourceException | ValidationException e) {
            throw new BadRequestRestException(e.getMessage());
        }
    }
    //get a specific user and the all contacts
    @GetMapping(("/users/{id}/contacts"))
    public  Collection<ContactDTO> findAContactsToSpecificUser(@PathVariable("id") long userId) {
        try {
            return contactService.getContactsByUserId(userId);
        } catch (NoSuchResourceException e) {
            throw new NotFoundRestException(e.getMessage());
        }
    }
}
