package com.denchsolutions.contacts.dto;

import java.util.List;

public final class ViewAsAdminUserDTO extends UserDTO {
    private Long id;
    private List<String> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public  List<String> getRoles() {
        return roles;
    }

    public void setRoles( List<String> roles) {
        this.roles = roles;
    }
}
