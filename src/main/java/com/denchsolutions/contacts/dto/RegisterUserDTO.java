package com.denchsolutions.contacts.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RegisterUserDTO {

    @NotBlank(message = "Username can not be blank.")
    @Size(min = 3, max = 30, message = "Username must be between 3 and 30.")
    private String username;

    @NotBlank(message = "Password is invalid.")
    @Size(min = 3, max = 50, message = "Password must be between 3 and 50.")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
