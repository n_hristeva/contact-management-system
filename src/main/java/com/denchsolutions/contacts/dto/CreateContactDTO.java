package com.denchsolutions.contacts.dto;
import com.denchsolutions.contacts.exception.ValidationException;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class CreateContactDTO {

    @NotBlank
    private String firstName;
    private String lastName;
    private String mobile;

    @Email
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        if (email != null && email.isEmpty() && email.isBlank()){
            throw new ValidationException("Mobile is invalid. Enter valid mobile or email.");
        }
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (mobile != null && mobile.isEmpty() && mobile.isBlank()){
            throw new ValidationException("Email is invalid. Enter valid mobile or email.");
        }
        this.email = email;
    }
}
