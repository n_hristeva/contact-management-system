package com.denchsolutions.contacts.dto;

import com.denchsolutions.contacts.exception.ValidationException;
import org.jetbrains.annotations.NotNull;

public final class UpdateAsAdminUserDTO extends UpdateUserDTO{
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(@NotNull String role) {
        if (role.equals("ADMIN")){
            throw new ValidationException("Role 'ADMIN' can not be set. Enter valid role.");
        }
        this.role = role;
    }
}
