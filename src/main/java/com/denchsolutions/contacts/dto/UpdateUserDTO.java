package com.denchsolutions.contacts.dto;

public class UpdateUserDTO {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
