package com.denchsolutions.contacts.dto;

import java.time.LocalDateTime;
import java.util.Collection;

public class ValidationErrorMessageDTO extends ErrorMessageDTO {
    private Collection<String> validationErrors;

    public ValidationErrorMessageDTO(LocalDateTime timestamp,
                                     String message,
                                     String path,
                                     Collection<String> validationErrors) {
        super(timestamp, message, path);
        this.validationErrors = validationErrors;
    }

    public Collection<String> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(Collection<String> validationErrors) {
        this.validationErrors = validationErrors;
    }
}
