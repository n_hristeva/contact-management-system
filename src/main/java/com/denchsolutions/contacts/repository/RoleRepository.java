package com.denchsolutions.contacts.repository;

import com.denchsolutions.contacts.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    /**
     * @param roleName Role to be found by name
     * @return instance ot Role
     */
    Role findByName(String roleName);

    /**
     * @param roleName Role to be found by name
     * @return True if found or False is not
     */
    boolean existsRoleByName(String roleName);
}
