package com.denchsolutions.contacts.repository;

import com.denchsolutions.contacts.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * @param username User to be found by username
     * @return instance of the User
     */
    User findByUsername(String username);

    /**
     * @param username User to be found by username
     * @return True is found False if not
     */
    boolean existsByUsername(String username);

    /**
     * Only by Admin 
     * @return all users in DB
     */
    @Override
    @NotNull List<User> findAll();
}
