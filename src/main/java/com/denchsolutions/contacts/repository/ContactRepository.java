package com.denchsolutions.contacts.repository;

import com.denchsolutions.contacts.model.Contact;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Collection;
import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    /**
     * @param userId Find all contacts by User id
     * @return All the User Contacts
     */
    Collection<Contact> findByUserId(long userId);

    /**
     *Only used by Admin
     * @return All Contacts in the DB table
     */
    @Override
    @NotNull List<Contact> findAll();

    /**
     * @param contactId Contact to be found
     * @param userId User details
     * @return Found instance of the Contact
     */
    Contact findContactByIdAndUserId(long contactId, long userId);

    /**
     * Only by Admin
     * @param contactId Contact to be found, looks at all the DB table without restrictions
     * @return Found instance of the Contact
     */
    Contact findContactById(long contactId);

    /** TODO --> Query not working ?!
     * When using this method Contact instance will be locked.
     * @param contactId Contact id to be found
     * @return Locked Contact instance
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT x FROM Contact x WHERE x.id = :contactId")
    Contact findContactByIdForUpdate(@Param("contactId") Long contactId);

    /**
     * @return list of all contact ids in DB
     */
    @Query("SELECT id FROM Contact")
    List<Long> findAllIds();
}