package com.denchsolutions.contacts.advice;

import com.denchsolutions.contacts.dto.ErrorMessageDTO;
import com.denchsolutions.contacts.dto.ValidationErrorMessageDTO;
import com.denchsolutions.contacts.exception.BadRequestRestException;
import com.denchsolutions.contacts.exception.NotFoundRestException;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    //404 NOT FOUND
    @ExceptionHandler(NotFoundRestException.class)
    protected ResponseEntity<Object> handleNotFound(RuntimeException exception, HttpServletRequest request) {
        return generateResponseEntity(HttpStatus.NOT_FOUND, exception.getMessage(), request.getRequestURI());
    }

    //400 BAD REQUEST
    @ExceptionHandler(BadRequestRestException.class)
    protected ResponseEntity<Object> handleBadRequest(RuntimeException exception, HttpServletRequest request) {
        return generateResponseEntity(HttpStatus.BAD_REQUEST, exception.getMessage(), request.getRequestURI());
    }

    @Override
    protected @NotNull ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException exception,
        @NotNull HttpHeaders headers,
        @NotNull HttpStatus status,
        @NotNull WebRequest request
    ) {
        final List<String> errors = new ArrayList<>();
        exception.getBindingResult()
            .getFieldErrors()
            .forEach(fieldError -> errors.add(fieldError.getField() + ": " + fieldError.getDefaultMessage()));
        exception.getBindingResult()
            .getGlobalErrors()
            .forEach(objectError -> errors.add(objectError.getObjectName() + ": " + objectError.getDefaultMessage()));

        return new ResponseEntity<>(
            new ValidationErrorMessageDTO(
                LocalDateTime.now(ZoneOffset.UTC),
                "VALIDATION_ERROR_OCCURRED",
                ((ServletWebRequest) request).getRequest().getRequestURI(),
                errors),
            HttpStatus.BAD_REQUEST
        );
    }

    private ResponseEntity<Object> generateResponseEntity(HttpStatus httpStatus, String errorMessage, String requestURI) {
        final LocalDateTime localDateTime = LocalDateTime.now(ZoneOffset.UTC);
        return new ResponseEntity<>(
            new ErrorMessageDTO(localDateTime, errorMessage, requestURI),
            httpStatus);
    }
}
