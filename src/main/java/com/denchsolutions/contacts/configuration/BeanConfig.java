package com.denchsolutions.contacts.configuration;

import com.denchsolutions.contacts.repository.ContactRepository;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Configuration
public class BeanConfig {
    private final RestTemplate restTemplate;
    private final ContactRepository contactRepository;

    public BeanConfig(RestTemplate restTemplate, ContactRepository contactRepository) {
        this.restTemplate = restTemplate;
        this.contactRepository = contactRepository;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        String hierarchy = "ADMIN > USER_WRITE \n USER_WRITE > USER_READ";
        roleHierarchy.setHierarchy(hierarchy);
        return roleHierarchy;
    }

    //Kafka Config
    //Simulate clicks
    @Bean
    CommandLineRunner commandLineRunner() {
        return args -> {
            List<Long> contactIds = contactRepository.findAllIds();
            System.out.println(contactIds);
            for (Long contactId : contactIds) {
               restTemplate.postForLocation("http://localhost:8080/api/contacts/{id}/click",null, contactId);
            }
        };
    }
}
