package com.denchsolutions.contacts.configuration;

import com.denchsolutions.contacts.service.ContactService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {
    ContactService contactService;

    public KafkaListeners(ContactService contactService) {
        this.contactService = contactService;
    }

    @KafkaListener(topics = "userTopic", groupId = "usersGroupId")
    void listener(Long contactId) {
        System.out.println("contact ID: " + contactId);
        contactService.updateContactClickCount(contactId);
    }
}
