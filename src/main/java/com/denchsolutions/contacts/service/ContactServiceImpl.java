package com.denchsolutions.contacts.service;

import com.denchsolutions.contacts.dto.ContactDTO;
import com.denchsolutions.contacts.dto.CreateContactDTO;
import com.denchsolutions.contacts.dto.UpdateContactDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminContactDTO;
import com.denchsolutions.contacts.exception.NoSuchResourceException;
import com.denchsolutions.contacts.exception.ValidationException;
import com.denchsolutions.contacts.model.Contact;
import com.denchsolutions.contacts.model.User;
import com.denchsolutions.contacts.repository.ContactRepository;
import com.denchsolutions.contacts.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ContactServiceImpl implements ContactService {
    private final UserRepository userRepository;
    private final ContactRepository contactRepository;
    private final ModelMapper modelMapper;

    public ContactServiceImpl(UserRepository userRepository, ContactRepository contactRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.contactRepository = contactRepository;
        this.modelMapper = modelMapper;
    }

    //called by User or Contact controller
    @Override
    public Collection<ContactDTO> getContactsByUserId(long userId) {
        Collection<Contact> contacts = contactRepository.findByUserId(userId);

        if (contacts.isEmpty()) {
            throw new NoSuchResourceException("No contacts found.");
        }

        return contacts.stream()
            .map(contact -> modelMapper.map(contact, ContactDTO.class))
            .toList();
    }

    @Override
    public void createContact(CreateContactDTO createContactDTO, Long userId) {
        Contact contact = modelMapper.map(createContactDTO, Contact.class);
        User user = userRepository.findById(userId)
            .orElseThrow(() -> new NoSuchResourceException("User does not exist."));

        contact.setUser(user);

        try {
            contactRepository.save(contact);
        } catch (DataIntegrityViolationException e) {
            throw new ValidationException("Contact with that email/mobile already exists.");
        }
    }

    @Override
    public void updateContact(UpdateContactDTO updateContactDTO, Long userId, Long contactId) {
        Contact foundContact = contactRepository.findContactByIdAndUserId(contactId, userId);
        modelMapper.map(updateContactDTO, foundContact);

        contactRepository.save(foundContact);
    }

    @Override
    public void deleteContact(Long userId, Long contactId) {
        Contact foundContact = contactRepository.findContactByIdAndUserId(contactId, userId);
        contactRepository.delete(foundContact);
    }

    //called by Admin controller
    @Override
    public Collection<ViewAsAdminContactDTO> getContactsAsAdmin() {
        Collection<Contact> contacts = contactRepository.findAll();

        if (contacts.isEmpty()) {
            throw new NoSuchResourceException("No contacts found.");
        }
        return contacts.stream()
            .map(contact -> {
                ViewAsAdminContactDTO result = modelMapper.map(contact, ViewAsAdminContactDTO.class);
                result.setUserId(contact.getUser()
                                     .getId());
                return result;
            })
            .toList();
    }

    @Override
    public ContactDTO findContactById(Long contactId) {
        Contact foundContact = contactRepository.findContactById(contactId);
        return modelMapper.map(foundContact, ContactDTO.class);
    }

    @Override
    public void deleteContactByContactId(Long contactId) {
        contactRepository.deleteById(contactId);
    }

    @Override
    public void updateContactByContactId(UpdateContactDTO updateContactDTO, Long contactId) {
        Contact foundContact = contactRepository.findContactById(contactId);
        modelMapper.map(updateContactDTO, foundContact);
        contactRepository.save(foundContact);
    }

    @Override
    public void updateContactClickCount(Long contactId) {
        Contact foundContact = contactRepository.findContactById(contactId);
        foundContact.setTotalClicks();
        contactRepository.save(foundContact);
        //TODO --> Query not working
       /* Contact contact = contactRepository.findContactByIdForUpdate(contactId);
        *increments by one each time
        *contact.setTotalClicks();
        *contactRepository.save(contact);
        */
    }
}
