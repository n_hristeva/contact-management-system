package com.denchsolutions.contacts.service;

import com.denchsolutions.contacts.dto.RegisterUserDTO;
import com.denchsolutions.contacts.dto.UpdateAsAdminUserDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminUserDTO;
import com.denchsolutions.contacts.exception.NoSuchResourceException;
import com.denchsolutions.contacts.model.Contact;
import com.denchsolutions.contacts.model.Role;
import com.denchsolutions.contacts.model.User;
import com.denchsolutions.contacts.repository.ContactRepository;
import com.denchsolutions.contacts.repository.RoleRepository;
import com.denchsolutions.contacts.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final ContactRepository contactRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, ContactRepository contactRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.contactRepository = contactRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }
    //create user with right USER_WRITE - create, read, update, delete
    @Override
    public User registerUser(RegisterUserDTO registerUserDTO) {
        final User user = new User(registerUserDTO.getUsername(), passwordEncoder.encode(registerUserDTO.getPassword()));
        user.getRoles().add(roleRepository.findByName("USER_WRITE"));
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Collection<ViewAsAdminUserDTO> getUsersAsAdmin() {
        Collection<User> users = userRepository.findAll();

        if (users.isEmpty()) {
            throw new NoSuchResourceException("No contacts found.");
        }

        return users.stream()
            .map(user -> modelMapper.map(user, ViewAsAdminUserDTO.class))
            .toList();
    }

    @Override
    public ViewAsAdminUserDTO findUserAsAdmin(long userId) {
        User foundUser = userRepository.findById(userId)
            .orElseThrow(() -> new NoSuchResourceException("User does not exist."));

        ViewAsAdminUserDTO result = modelMapper.map(foundUser, ViewAsAdminUserDTO.class);
        result.setRoles(foundUser.getRoles()
                            .stream()
                            .map(Role::getName)
                            .toList());
        return result;
    }
    // before deleting a user we need to delete the contacts related.
    @Override
    public void deleteUserByUserId(long userId) {
        Collection<Contact> contacts = contactRepository.findByUserId(userId);
        contactRepository.deleteAll(contacts);
        userRepository.deleteById(userId);
    }
    //update on user role should be done only by admin
    @Override
    public void updateUserByUserId(UpdateAsAdminUserDTO adminUserDTO, long userId) {
        User foundUser = userRepository.findById(userId).orElseThrow(() -> new NoSuchResourceException("User does not exist."));

        if (roleRepository.existsRoleByName(adminUserDTO.getRole())){
            modelMapper.map(adminUserDTO, foundUser);
            userRepository.save(foundUser);
        }
    }
}
