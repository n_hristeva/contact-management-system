package com.denchsolutions.contacts.service;

import com.denchsolutions.contacts.dto.RegisterUserDTO;
import com.denchsolutions.contacts.dto.UpdateAsAdminUserDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminUserDTO;
import com.denchsolutions.contacts.model.User;

import java.util.Collection;

public interface UserService {

    /**
     * @param username Is the name of the User
     * @return true or false
     */
    boolean existsByUsername(String username);

    /**
     * @return Created User
     */
    User registerUser(RegisterUserDTO registerUserDTO);

    /**
     * @param username Find by provided username
     * @return User instance
     */
    User findByUsername(String username);

    /**
     * @return Find all users in DB
     */
    Collection<ViewAsAdminUserDTO> getUsersAsAdmin();

    /**
     * @param userId User to be found
     * @return Information of the user that should be seen only by Admin
     */
    ViewAsAdminUserDTO findUserAsAdmin(long userId);

    /**By deleting User we delete his contacts too
     * @param userId User to be removed from DB
     */
    void deleteUserByUserId(long userId);

    /**
     * @param adminUserDTO Information that can be updated by Admin
     * @param userId User to be updated
     */
    void updateUserByUserId(UpdateAsAdminUserDTO adminUserDTO, long userId);
}
