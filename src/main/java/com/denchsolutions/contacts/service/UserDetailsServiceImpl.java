package com.denchsolutions.contacts.service;

import com.denchsolutions.contacts.model.Role;
import com.denchsolutions.contacts.model.User;
import com.denchsolutions.contacts.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final String ROLE_PREFIX = "ROLE_";

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {this.userRepository = userRepository;}

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("username " + username + " not found");
        }

        return new org.springframework.security.core.userdetails.User(
            user.getUsername(), user.getPassword(),
            getSimpleGrantedAuthorities(user.getRoles())
        );
    }

    private List<SimpleGrantedAuthority> getSimpleGrantedAuthorities(Set<Role> roles) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role.getName()));
        }

        return authorities;
    }
}
