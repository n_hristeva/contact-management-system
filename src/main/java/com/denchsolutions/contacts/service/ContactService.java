package com.denchsolutions.contacts.service;

import com.denchsolutions.contacts.dto.ContactDTO;
import com.denchsolutions.contacts.dto.CreateContactDTO;
import com.denchsolutions.contacts.dto.UpdateContactDTO;
import com.denchsolutions.contacts.dto.ViewAsAdminContactDTO;

import java.util.Collection;

public interface ContactService {
    /** Info provided only for Admin
     * @return All contacts with additional info for each user
     */
    Collection<ViewAsAdminContactDTO> getContactsAsAdmin();

    /**Look at the DB table without using restriction by user
     * Only for Admin
     * @param contactId Contact to be found
     * @return Found Contact
     */
    ContactDTO findContactById(Long contactId);

    /**Only by Admin
     * @param contactId Remove Contact from table without needing info from its User
     */
    void deleteContactByContactId(Long contactId);

    /**
     * @param updateContactDTO Details that are requested to be updated
     * @param contactId Contact to be updated
     */
    void updateContactByContactId(UpdateContactDTO updateContactDTO, Long contactId);

    /**
     * @param userId Provided user id
     * @return Information for the contact
     */
    Collection<ContactDTO> getContactsByUserId(long userId);

    /**
     * @param createContactDTO Requested details used for creating a contact
     * @param userId User that will have the contact
     */
    void createContact(CreateContactDTO createContactDTO, Long userId);

    /**
     * @param updateContactDTO Requested details for update
     * @param userId User that wishes to update
     * @param contactId Contact that wants to be updated
     */
    void updateContact(UpdateContactDTO updateContactDTO, Long userId, Long contactId);

    /**
     * @param userId User that want to remove a contact
     * @param contactId Contact that wants to be removed
     */
    void deleteContact(Long userId, Long contactId);

    /**
     * @param contactId Contact that will be updated only by total_clicks
     */
    void updateContactClickCount(Long contactId);
}
