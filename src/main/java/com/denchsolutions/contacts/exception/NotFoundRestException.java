package com.denchsolutions.contacts.exception;

public class NotFoundRestException extends RuntimeException {
    public NotFoundRestException(String message) {
        super(message);
    }
}
