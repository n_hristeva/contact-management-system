package com.denchsolutions.contacts.exception;

public class BadRequestRestException extends RuntimeException {
    public BadRequestRestException(String message) {
        super(message);
    }
}
