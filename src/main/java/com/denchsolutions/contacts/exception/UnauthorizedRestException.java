package com.denchsolutions.contacts.exception;

public class UnauthorizedRestException extends RuntimeException {

    public UnauthorizedRestException() {}

    public UnauthorizedRestException(String message) {
        super(message);
    }
}
