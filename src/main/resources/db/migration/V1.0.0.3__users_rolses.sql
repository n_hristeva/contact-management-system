CREATE TABLE IF NOT EXISTS users_roles
(
    user_id     BIGSERIAL NOT NULL,
    role_id     BIGSERIAL NOT NULL,
    CONSTRAINT users_roles_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT fk_roles FOREIGN KEY (role_id) REFERENCES roles (id) MATCH FULL,
    CONSTRAINT fk_users FOREIGN KEY (user_id) REFERENCES users (id) MATCH FULL
);