CREATE TABLE IF NOT EXISTS contacts
(
    id           BIGSERIAL PRIMARY KEY,
    first_name   VARCHAR(30)                        NOT NULL,
    last_name    VARCHAR(30),
    mobile       VARCHAR(30),
    email        VARCHAR(50),
    total_clicks BIGINT DEFAULT 0,
    user_id      BIGINT,
    CONSTRAINT FK_CONTACTS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id)
    );

CREATE TABLE IF NOT EXISTS  users
(
    id       BIGSERIAL PRIMARY KEY,
    username VARCHAR(30)                            NOT NULL,
    password VARCHAR(30)                            NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id),
    CONSTRAINT ux_user_username UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS  roles
(
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(30)                                NOT NULL,
    CONSTRAINT pk_roles PRIMARY KEY (id),
    CONSTRAINT ux_role_name UNIQUE (name)
);